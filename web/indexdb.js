
var db;
async function openIndexDb(callback) {
    let request = indexedDB.open('store', 1);

    request.onerror = function () {
        console.error("Error", openRequest.error);
        callback(true);
    };

    request.onsuccess = function () {
        db = request.result;
        callback(false);
    };

    request.onupgradeneeded = function () {
        db = request.result;
        if (!db.objectStoreNames.contains('breweriesList')) {
            createObjectStore(db);
            callback(false);
        }
        else
        {
            callback(true);
            console.log('Does contain breweries');
        }
    }
}



function createObjectStore(dbInstance) {
    if (!dbInstance.objectStoreNames.contains('breweriesList')) {
        dbInstance.createObjectStore('breweriesList', { keyPath: 'id' });
        console.log("Object Store created");
    }
}


function saveDataToDatabase(breweries) {
    var request = db.transaction("breweriesList", "readwrite")
        .objectStore("breweriesList")
        .add({ id: 1, value: breweries });

        request.onsuccess = function(event) {
            console.log("Data Stored successfully");
        }
        request.onerror = function(event) {
            console.log("Data Store faced error");
        }
}



function getDataFromDatabase(callback) {
    var objectStore = db.transaction("breweriesList", "readwrite").objectStore("breweriesList");
    objectStore.openCursor().onsuccess = function (event) {
    var result = event.target.result;
    if (result) {
        callback(cursor.value["value"]);
    }
    };
}

