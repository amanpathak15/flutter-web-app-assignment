import 'package:flutter/material.dart';
import 'package:flutter_web_app/views/listPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Web app',
      theme: ThemeData(
        backgroundColor: Colors.cyan,
        primarySwatch: Colors.green,
      ),
      home: ListPage(),
    );
  }
}

