import 'dart:convert';
import 'dart:js' as js;
import '../model/listItem.dart';


class DatabaseHelper {
  static final DatabaseHelper _instance =
  DatabaseHelper._internal();
  DatabaseHelper._internal();
  static DatabaseHelper getInstance() => _instance;

  createdatabase(Function(dynamic) callback) {
    js.context.callMethod('openIndexDb', [callback]);
  }

  saveDataToDatabase(List<ListItem> list) {
    js.context.callMethod('saveDataToDatabase',
        [json.encode(List<dynamic>.from(list.map((x) => x.toJson())))]);
  }

  getDataFromDatabase(Function(dynamic) callback) {
    js.context.callMethod('getDataFromDatabase', [callback]);
  }
}