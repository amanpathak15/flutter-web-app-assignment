import 'dart:async';
import 'package:http/http.dart' as http;

const baseUrl = "https://api.openbrewerydb.org";

class Api {
  static Future getPosts() {
    var url = baseUrl + "/breweries";
    return http.get(url);
  }
}