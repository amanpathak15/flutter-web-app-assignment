import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_web_app/database/databasehelper.dart';
import 'package:flutter_web_app/model/listItem.dart';
import 'package:flutter_web_app/repo/Repo.dart';
import 'package:flutter_web_app/services/api.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../model/listItem.dart';

class ListPage extends StatefulWidget{

  ListPage();

  @override
  State<StatefulWidget> createState() {

    return ListPageState();
  }

}


class ListPageState extends State<ListPage>{
  Repo repo;
  var list = new List<ListItem>();
  bool isListLoaded = false;


  @override
  void initState() {
    super.initState();
    repo = Repo();
    repo.getData((data){
      setState(() {
        list = data as List;
        isListLoaded = true;
      });
    });
  }





    @override
    Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Breweries (" + list.length.toString() + ")"),
          ),
          body: Container(
              color: Colors.cyan,
              child: isListLoaded ? ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return Card(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: ListTile(
                          title: Text(list[index].name ?? 'Default',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                              )
                          ),
                          isThreeLine: true,
                          subtitle: Text("Type : "+ list[index].breweryType + "\n" + list[index].country + "-" + list[index].state, style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),),
                          leading: Text((index + 1).toString(),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                              )
                          ),
                        ),
                      )
                  );
                },
              ) :
              Center(
                child: Text('Loading the Assignment..',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17
                  ),
                ),
              )
          )
      );
    }








}
  
  
  
  
  
