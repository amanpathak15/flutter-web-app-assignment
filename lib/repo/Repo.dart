import 'dart:convert';

import 'package:flutter_web_app/database/databasehelper.dart';
import 'package:flutter_web_app/model/listItem.dart';
import 'package:flutter_web_app/services/api.dart';
import '../model/listItem.dart';

class Repo {
  static Repo _repo;
  DatabaseHelper databaseHelper;

  Repo._createInstance();
  factory Repo() {
    if(_repo == null){
      _repo = Repo._createInstance();
    }
    return _repo;
  }



   _getPostFromService(Function(dynamic) returnFunction)  async {
    List<ListItem> list;
    var response = await Api.getPosts();
    if(response.statusCode == 200){

      Iterable iterable = json.decode(response.body);
      list = iterable.map((model) => ListItem.fromJson(model)).toList();
      databaseHelper.saveDataToDatabase(list);
      returnFunction(list);
    }
    else
    {
      throw Exception('Failed to load');
    }
  }

    saveDataToDb(List<dynamic> list) async {
      await databaseHelper.saveDataToDatabase(list);
    }


     getData(Function(dynamic) returnFunction) async{
      List<ListItem> list;
      databaseHelper = DatabaseHelper.getInstance();
      databaseHelper.createdatabase((data) {
         if (data) {
           databaseHelper.getDataFromDatabase((data) {

             list = (json.decode(data) as List)
                 .map((mydata) => new ListItem.fromJson(mydata))
                 .toList();
             returnFunction(list);
           });
         }
         else {
           _getPostFromService(returnFunction);
         }

      });
      }



  }









